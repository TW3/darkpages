// ==UserScript==
// @name          Dark Pages
// @author        TW3
// @icon          https://glcdn.githack.com/TW3/darkpages/raw/master/img/dp.png
// @namespace     org.browser.bhawk
// @description	  Turns all html pages light on dark to help reduce eye strain. https://gitlab.com/TW3/darkpages
// @copyright     2018, stormi (https://userstyles.org/styles/31267)
// @license       CC BY-NC-SA-4.0; https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
// @version       0.10
// @updateURL     https://glcdn.githack.com/TW3/darkpages/raw/master/src/DarkPages.js
// @run-at        document-end
// @grant         GM_addStyle
// @include       http*
// @exclude       browser*
// @exclude       *tw3.gitlab.io/*
// ==/UserScript==
(function () {
	var cssText = `/*----- @media(prefers - color - scheme: dark) is unavailable until Chromium 76 -----*/
		@media only screen {
		body, :not(iframe > *) {background-color: #313133 !important}
		div:first-child {background-color: #313133}
		nav div {background-color: #313133}
		header div, aside {background-color: #313133 !important}
		/*----- SITE SPECIFIC FIXES - Please add any sites via @exclude (above) if they are already dark by default. Thanks! -----*/
		div:first-child.user-watcher, main.tw-flex div, div.twitch_light, div.twitch_light > div, div.drag-boundary {background: transparent !important; z-index: 997} /* Twitch.tv - iframes within iframes.. what a mess.. */
		div.player-controls{z-index: 999 !important} /* More Twitch.tv */
		main > div:first-child, div.drag-boundary, div.hide, div#app-container, div#root, div.viewer, div.component {background: transparent !important} /* More Twitch.tv */
		div.features-and-analysis, div.container-heron, div.b-g-p, div.navp-wrapper, div.orb-footer-inner {background: transparent !important} /* BBC.co.uk - another mess.. */
		div.main__slice {background-color: transparent !important} /* More BBC.co.uk */
		div.top-story__wrapper, div.cmts-ft, div.orb-footer, div#site-container {background-color: #313133 !important} /* More BBC.co.uk */
		div.js-breaking-news-banner{visibility: hidden !important; display: none !important} /* More BBC.co.uk - Annoying breaking news banner */
		.site-wrapper, .header-wrap--home, div#header_wrapper, div.footer {background-color: #313133 !important} /* duckduckgo.com */
		ytd-app[is-watch-page], ytd-masthead, a.yt-simple-endpoint{background-color: #313133 !important} /* youtube.com */
		body > div#viewport, body > div#main {background-color: #313133 !important} /* google.com */
		div.m-page-wrapper, article.m-story, header.m-header, footer.m-footer, section.m-sub-navigation, section.m-card-group-container, section.m-tile-hub {background-color: #313133 !important} /* si.com */
		div.nav-sidebar-inner-scroll {overflow-y: visible !important; overflow: visible !important} /* gitlab.com */
		div.nav-sidebar-inner-scroll > ul > li > ul {border: none !important} /* More gitlab.com */
		div.info-well {background-color: #313133 !important} /* More gitlab.com */
		/*----- DEFAULT TEXT, BORDER & BACKGROUND COLORS -----*/
		html {background: #313133 !important}
		img {border-radius: 1.5%}
		* {user-select: auto !important; color: #c2c2c2 !important; box-shadow: none !important; border-color, border-top-color, border-bottom-color, border-left-color, border-right-color: #444 !important}
		:not(iframe,img) {mix-blend-mode: difference}
		:not(img, span) {background-color: #313133}
		*:before, *:after {background-color: transparent !important; background-image: none !important}
		iframe, html:first-child {background: transparent !important}
		figcaption, nav {background-color: #313133 !important}
		article, section, section div:first-of-type {background-color: transparent !important}
		button {background-color: transparent !important}
		/*----- LINKS -----*/
		a, a * {color: grey !important; text-decoration: transparent !important}
		a:hover, a:hover *, a:visited:hover, a:visited:hover *, span[onclick]:hover, div[onclick]:hover, [role="link"]:hover, [role="link"]:hover *, [role="button"]:hover *, [role="menuitem"]:hover, [role="menuitem"]:hover *, .link:hover, .link:hover * {
		    color: #00A3DD !important}
		a:visited, a:visited * {color: #607069 !important}
		a.highlight, a.highlight *, a.active, a.active *, .selected, .selected *, [href="#"] {color: #DDD !important; font-weight: bold !important}
		/*----- HEADERS -----*/
		h1, h2, h3, h4, h5, h6, h1 *, h2 *, h3 *, strong, span, ul, li, [id*="headline"], [class*="headline"], [id*="header"], [class*="header"], [class*="header"] td {
		    color: #DDD !important; background-color: transparent !important}
		a h1, a h2, a h3, a h4, a h5, a h6, h1 a, h2 a, h3 a, a strong, a[id*="headline"], a[class*="headline"], a[id*="header"], a[class*="header"] {
		    text-decoration: bold !important}
		li a{background-color: transparent !important}
		[class*="error"], [class*="alert"], code, span[onclick], div[onclick] { color: #900 !important}
		::selection {background: #00A3DD !important; color: white !important}
		:focus {outline: none !important}
		table {background: rgba(40,30,30,.6) !important; border-radius: 6px !important}
		table > tbody > tr:nth-child(even), table > tbody > tr > td:nth-child(even) {background-color: rgba(0,0,0,.2) !important}
		thead, thead > tr, thead > tr > th {background-color: #313133 !important}
		embed, label [onclick], nav ul, div[style*="position:"][style*="left:"][style*="visible"], div[style*="z-index:"][style*="left:"][style*="visible"], div[style*="-moz-user-select"], div[role="menu"], div[role="dialog"], span[class*="script"] div, [id*="menu"], [id*="Menu"], [class*="dropdown"], [class*="popup"], [class="title"], ul[style*="display:"], ul[style*="visibility:"] ul, [id*="nav"] ul, [class*="nav"] ul, ul[class*="menu"], a[onclick][style*="display"], #translator-popup, .menu, .tooltip, .hovercard, .vbmenu_popup {
		    background: transparent !important}
		header, #header, footer, #footer {background: #313133 !important}
		body > #dialog, body > .xenOverlay {
		    background: rgba(19,19,19,.96) !important; background-clip: padding-box !important;
		    box-shadow: 0 0 15px #000, inset 0 0 0 1px rgba(200,200,200,.5), inset 0 0 5px #111 !important;
		    border: 10px solid rgba(99,99,99,.7) !important; border-radius: 0 !important}
		[id*="overlay"], [id*="lightbox"], blockquote {background-color: rgba(35,35,35,.9) !important; border-radius: 5px}
		pre, dl, .Message code {background-color: transparent !important}
		/*----- DEFAULT BUTTONS, SEARCH BOXES & CO -----*/
		form, input {background: transparent !important; color: #fff !important}
		select, button, [role="button"], a.button, a.submit, a.BigButton, a.TabLink, .install[onclick] {
		    -webkit-appearance: none !important;transition: border-color 0.3s !important;
		    background: transparent !important; color: #c2c2c2 !important}
		a[href="javascript:;"], a[class*="button"]:not(:empty), a[id*="button"]:not(:empty), a[id*="Button"]:not(:empty), div[class*="button"][onclick] {
		    transition: border-color 0.3s !important; background: transparent !important; color: #c2c2c2 !important;
		    text-shadow: 0 1px #000 !important; border-color: transparent !important; box-shadow: none !important}
		a[href="javascript:;"]:hover, a[class*="button"]:not(:empty):hover, a[id*="button"]:hover, a[id*="Button"]:not(:empty):hover, div[class*="button"][onclick]:hover {
		    background: transparent !important; color: #00A3DD !important}
		input *, select *, button *, a.button *, a.submit * {color: #c2c2c2 !important; text-shadow: none !important}
		input:hover, input[type="button"]:hover, select:hover, button:hover, [role="button"]:hover, a.button:hover, a.submit:hover, a.BigButton:hover, a.TabLink:hover {
		    border: none !important}
		input:focus, select:focus {box-shadow: none !important}
		input *:hover * {color: #F0F0F0 !important; text-shadow: none !important}
		input[disabled], select[disabled], button[disabled], input[disabled]:hover, select[disabled]:hover, button[disabled]:hover, input[disabled]:focus, select[disabled]:focus, button[disabled]:focus {
		    opacity: 0.5 !important; border-color: #333 !important}
		input[type="checkbox"] {border-radius: 1px !important}
		input[type="radio"], input[type="radio"]:focus {border-radius: 100% !important}
		input[type="checkbox"], input[type="radio"] {min-width: 12px; min-height: 12px}
		input[type="checkbox"]:checked, input[type="radio"]:checked {border-color: #077 !important; box-shadow: 0 0 5px #077 !important}
		select {padding-right: 15px !important; background: 4px center #474749 !important; transition: border-color 0.3s, background-position 0.3s !important}
		button:active, input[type="submit"]:active, input[type="button"]:active, a.button:active, a[class*="button"]:not(:empty):active, a.submit:active, a.BigButton:active, a.TabLink:active, .Active .TabLink {
			background: #292929 !important; color: #FFF !important}
		textarea {-webkit-appearance: none !important; background: rgba(0,0,0,.3) !important; border-radius: 3px !important; border: 1px solid #000 !important; box-shadow: inset 0 0 8px #000 !important; transition: border-color, background, 0.3s !important}
		textarea, textarea * {color: #C8C8C8 !important; text-shadow: 0 0 1px gray !important}
		textarea:hover, textarea:focus:hover {border-color: #333 !important}
		textarea:focus {background: rgba(0,0,0,.5) !important; border-color: #222 !important}
		textarea:focus, textarea:focus > * {text-shadow: none !important; box-shadow: none !important}
		option, optgroup {-webkit-appearance: none !important; background: none !important; color: #666 !important}
		optgroup {background: #222 !important; color: #DDD !important}
		option:not([disabled]):hover, option:focus, option:checked {background: linear-gradient(#333, #292929) !important; color: #DDD !important}
		/*----- IMAGES -----*/
		img {background: transparent none !important; background-color: transparent !important; opacity: none; transition: opacity .2s}
		img:hover, a:hover img, #mpiv-popup {opacity: 1 !important}
		body, *:not(:empty):not(html):not(span):not(a):not(b):not(option):not(select):not(img):not([style="display: block;"]):not([onclick*="open"]):not([onclick*="s_objectID"]):not([class*="stars"]):not([id*="stars"]):not([id="rating"]):not([class="rating"]):not([class*="SPRITE"]):not([id*="SPRITE"]):not([class*="item"]):not([id*="item"]):not([class*="thumb"]):not([class*="icon"]):not([class*="photo"]):not(.view):not(.text):not([id*="lbImage"]):not([class*="cc-in"]):not([class*="gr-body"]):not([id*="watch"]):not(#globalsearch),
		:not(form:first-child), .r3_hm, .gmbutton2 b, .gtab-i, .ph, .bstab-iLft, .csb, #pagination div, [style*="sprite2.png"], #mw-head-base, #mw-page-base {background-image: none !important; background-color: transparent !important}
		}`;

	if (typeof GM_addStyle != "undefined") {
		GM_addStyle(cssText);
	}
})();