
![DarkPages Logo](img/DarkPages.png)

# DarkPages

<hr/>

## A nightmode/darkmode/dark reader clone for [Greasemonkey][ce8badb9].

  [ce8badb9]: https://www.greasespot.net/ "Greasemonkey"

This repository holds the source code for DarkPages. The preferred method of linking to and obtaining a copy of the script is via [Githack][f254f6e3]:

  [f254f6e3]: https://raw.githack.com/ "Githack"

### [DarkPages.js][e19f2d1f]

  [e19f2d1f]: https://glcdn.githack.com/TW3/darkpages/raw/master/src/DarkPages.js "DarkPages.js"

<hr/>

## About

DarkPages is a [Greasemonkey][ce8badb9] user script which turns all websites dark. The effect is designed to help reduce eye strain. The result also has the added benefit of decreasing display hardware energy use.

<hr/>

### Screenshots:

<br/>

![Screenshot 1](img/1.png)

<br/>

![Screenshot 3](img/3.png)

<br/>

![Screenshot 2](img/2.png)

<hr/>

## License

The DarkPages user script is a derivative work. The original script is © 2018, [stormi][0d2cff9e]. DarkPages is licensed via the [Creative Commons Attribution-NonCommercial-ShareAlike public license][4299ec53].

  [4299ec53]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "CC BY-NC-SA"
  [0d2cff9e]: https://userstyles.org/styles/31267 "stormi"
